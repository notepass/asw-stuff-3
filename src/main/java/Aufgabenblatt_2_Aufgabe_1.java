import java.sql.*;

/**
 * Testdata:
 * first | second
 *     1 | 2
 *     2 | 3
 *     3 | 4
 *     2 | 5
 *
 * Expected result:
 * [/][0][1][2][3][4]
 * [0][ ][ ][ ][ ][ ]
 * [1][X][ ][ ][ ][ ]
 * [2][X][X][ ][ ][ ]
 * [3][X][X][X][ ][ ]
 * [4][X][X][ ][ ][ ]
 *
 * 1 | 2
 * 1 | 3
 * 2 | 3
 * 1 | 4
 * 2 | 4
 * 3 | 4
 * 1 | 5
 * 2 | 5
 */

public class Aufgabenblatt_2_Aufgabe_1 {
    public static void main(String[] args) throws SQLException {
        //Verbindung zur Datenbank aufbauen
        Connection con = DriverManager.getConnection("jdbc:postgresql://localhost/KAPA", "postgres", "postgres");
        Statement statement = con.createStatement();

        //Stanndardwerte in die Tabelle schreiben (Da zum Schluss die berechneten Werte in die Tabelle geschrieben werden => reset)
        System.out.println("Filling table with default values");
        int[][] defaultValues = new int[4][2];
        defaultValues[0][0] = 1;
        defaultValues[0][1] = 2;
        defaultValues[1][0] = 2;
        defaultValues[1][1] = 3;
        defaultValues[2][0] = 3;
        defaultValues[2][1] = 4;
        defaultValues[3][0] = 2;
        defaultValues[3][1] = 5;

        statement.execute("TRUNCATE TABLE public.\"Bestandteil\"");
        for (int i = 0;i<defaultValues.length; i++) {
            statement.execute("INSERT INTO public.\"Bestandteil\" (first, second) VALUES ("+defaultValues[i][0]+", "+defaultValues[i][1]+")");
        }

        //Die größte nummer aus der Tabelle auslesen (da von 1 bis MAX gelaufen werden muss)
        ResultSet rs = statement.executeQuery("SELECT MAX(GREATEST(first, second)) FROM public.\"Bestandteil\"");
        rs.next();
        int kMax = rs.getInt(1); //Mit standarddaten 5

        //Initialisierung des ergebnis-arrays
        //Row (j), Column (i)
        boolean[][] ergebnistabelle = new boolean[kMax][kMax];

        //Alle reihen aus der Tabelle auslesen und in die ergebnistablle schreiben (Rote kreuze)
        rs = statement.executeQuery("SELECT * FROM public.\"Bestandteil\"");

        while (rs.next()) {
            ergebnistabelle[rs.getInt("second")-1][rs.getInt("first")-1] = true;
        }


        //Den im Aufgabenblatt beschriebenen algorythmus anwenden (Schwarze Kreuze)
        for (int k = 0;k<ergebnistabelle.length; k++) {
            for (int i = 0; i < ergebnistabelle[k].length; i++) {
                for (int j = 0; j < ergebnistabelle[k].length; j++) {
                    ergebnistabelle[i][j] = (ergebnistabelle[i][k] && ergebnistabelle[k][j]) || ergebnistabelle[i][j];
                }
            }
        }

        //Ausgabe der Tabelle auf der Konsole
        System.out.print("[/]");
        for (int i=0;i<ergebnistabelle.length;i++) {
            System.out.print("["+i+"]");
        }
        System.out.println();

        for (int i = 0;i<ergebnistabelle.length; i++) {
            System.out.print("["+i+"]");
            for (int j=0;j<ergebnistabelle[i].length;j++) {
                if (ergebnistabelle[i][j]) {
                    System.out.print("[X]");
                } else {
                    System.out.print("[ ]");
                }
            }

            System.out.println();
        }

        //Schreiben der berechneten Daten
        statement.execute("TRUNCATE TABLE public.\"Bestandteil\"");
        for (int i = 0;i<ergebnistabelle.length; i++) {
            for (int j = 0; j < ergebnistabelle[i].length; j++) {
                if (ergebnistabelle[i][j]) {
                    int first = j+1;
                    int second = i+1;
                    statement.execute("INSERT INTO public.\"Bestandteil\" (first, second) VALUES ("+first+", "+second+")");
                }
            }
        }
    }
}
