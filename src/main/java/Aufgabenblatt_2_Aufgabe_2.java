import java.sql.*;
import java.util.UUID;

public class Aufgabenblatt_2_Aufgabe_2 {
    private static long startTime = 0;

    public static void main(String[] args) throws SQLException {
        Connection con = DriverManager.getConnection("jdbc:postgresql://localhost/KAPA", "postgres", "postgres");

        //Clear table to avoid constraint violation
        con.createStatement().execute("TRUNCATE TABLE public.\"Bestandteil\"");

        //Definition der SQL-String für die "plaintext"-Verarbeitung
        String writeSql = "INSERT INTO public.\"Bestandteil\" (first, second) VALUES (%d, %d)";
        String readSql = "SELECT * FROM public.\"Bestandteil\" WHERE first = %d";

        //Definition der statement für die prepared-statement-verarbeitung
        PreparedStatement readStatement = con.prepareStatement("SELECT * FROM public.\"Bestandteil\" WHERE first = ?");
        PreparedStatement writeStatement = con.prepareStatement("INSERT INTO public.\"Bestandteil\" (first, second) VALUES (?, ?)");

        //Statement für die "plaintext" verarbeitung
        Statement stmnt = con.createStatement();

        //Definition von Strings für die ausgabe der Daten auf der Konsole
        String formatterString = "\n\n===== Operation: %s =====\nAnzahl Operationen: %d\nBenötigte Zeit: %dms\nBenötigte Zeit pro operation:%.2fms\n=====||=====\n";
        String operationReadStatement = "Lesen (Prepared statement)";
        String operationRead = "Lesen";
        String operationWriteStatement = "Schreiben (Prepared statement)";
        String operationWrite= "Schreiben";

        //Ausführen der Abfragen und ausgeben der Daten
        int numCount = 1000;

        System.out.print(formatData(formatterString, operationWriteStatement, benchmarkStatementWrite(writeStatement, numCount), numCount));
        System.out.print(formatData(formatterString, operationWrite, benchmarkWrite(stmnt, writeSql, numCount), numCount));

        System.out.print(formatData(formatterString, operationReadStatement, benchmarkStatementRead(readStatement, numCount), numCount));
        System.out.print(formatData(formatterString, operationRead, benchmarkRead(stmnt, readSql, numCount), numCount));
    }

    /**
     * Diese Methode führt ein lesendes prepared-statement so of wie angegeben aus und gibt die Laufzeit in Millisekunden zurück
     * @param statement Das auszuführende preapred-stament
     * @param numRunCount Wie oft das stament ausgeführt werden soll
     * @return Verarbeitungsdauer in ms
     * @throws SQLException
     */
    public static long benchmarkStatementRead(PreparedStatement statement, int numRunCount) throws SQLException {
        startTimeMeasure();

        for (int i=0;i<numRunCount;i++) {
            statement.setInt(1, numRunCount+i);
            statement.executeQuery();
        }

        return getTimeNeeded();
    }

    /**
     * Diese Methode führt ein schreibendes prepared-statement so of wie angegeben aus und gibt die Laufzeit in Millisekunden zurück
     * @param statement Das auszuführende preapred-stament
     * @param numRunCount Wie oft das stament ausgeführt werden soll
     * @return Verarbeitungsdauer in ms
     * @throws SQLException
     */
    public static long benchmarkStatementWrite(PreparedStatement statement, int numRunCount) throws SQLException {
        startTimeMeasure();

        for (int i=0;i<numRunCount;i++) {
            statement.setInt(1, numRunCount+i);
            statement.setInt(2, numRunCount+i);
            statement.execute();
        }

        return getTimeNeeded();
    }

    /**
     * Diese Methode führt ein lesendes statement so of wie angegeben aus und gibt die Laufzeit in Millisekunden zurück
     * @param statement Das auszuführende stament
     * @param numRunCount Wie oft das stament ausgeführt werden soll
     * @return Verarbeitungsdauer in ms
     * @throws SQLException
     */
    public static long benchmarkRead(Statement statement, String query, int numRunCount) throws SQLException {
        startTimeMeasure();

        for (int i=0;i<numRunCount;i++) {
            statement.executeQuery(String.format(query, 2*numRunCount+i));
        }

        return getTimeNeeded();
    }

    /**
     * Diese Methode führt ein schreibendes statement so of wie angegeben aus und gibt die Laufzeit in Millisekunden zurück
     * @param statement Das auszuführende stament
     * @param numRunCount Wie oft das stament ausgeführt werden soll
     * @return Verarbeitungsdauer in ms
     * @throws SQLException
     */
    public static long benchmarkWrite(Statement statement, final String query, int numRunCount) throws SQLException {
        startTimeMeasure();

        for (int i=0;i<numRunCount;i++) {
            statement.execute(String.format(query, 2*numRunCount+i, 2*numRunCount+i));
        }

        return getTimeNeeded();
    }

    /**
     * Startet die "Stoppuhr"
     */
    public static void startTimeMeasure() {
        startTime = System.currentTimeMillis();
    }

    /**
     * Gibt die Zeit auf der "Stoppuhr" zurück
     * @return
     */
    public static long getTimeNeeded() {
        return System.currentTimeMillis() - startTime;
    }

    /**
     * Formatiert den Ausgabestring
     * @param template String template
     * @param operation Operation
     * @param timeTaken Benötigte Zeit in Millisekunden
     * @param numCount Anzahl der ausführungen
     * @return
     */
    public static String formatData(String template, String operation, long timeTaken, int numCount) {
        return String.format(template, operation, numCount, timeTaken, (float)((float)timeTaken/(float)numCount));
    }
}
