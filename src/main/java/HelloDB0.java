import javax.swing.*;
import java.sql.*;

public class HelloDB0 {
    public static final String TEMPLATE_ROW = "\tRow %d:\n";
    public static final String TEMPLATE_COLUMN = "\t\tColumn %d: %s\n";

    public static void main(String[] args) throws SQLException {
        Connection con = DriverManager.getConnection("jdbc:postgresql://localhost/KAPA", "postgres", "postgres");
        Statement statement = con.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM artikel");

        System.out.println("Result");
        while (rs.next()) {
            System.out.printf(TEMPLATE_ROW, rs.getRow());
            for (int i=1; i<=rs.getMetaData().getColumnCount(); i++) {
                System.out.printf(TEMPLATE_COLUMN, i, rs.getString(i));
            }
        }
    }
}
